import path from 'path'
import webpack from 'webpack'
import { createFsFromVolume, Volume } from 'memfs'
import joinPath from 'memory-fs/lib/join'

export default (fixture, options = {}) => {
  const compiler = webpack({
    context: __dirname,
    entry: `./${fixture}`,
    output: {
      path: path.resolve(__dirname),
      filename: 'bundle.js',
    },
    module: {
      rules: [
        {
          test: /\.md$/,
          use: {
            loader: path.resolve(__dirname, '../src/loader.js'),
            options: {
              name: 'Alice',
            },
          },
        },
      ],
    },
  })

  // @see https://github.com/streamich/memfs/issues/404
  const fs = createFsFromVolume(new Volume())
  fs.join = joinPath
  compiler.outputFileSystem = fs
  compiler.resolvers.context.fileSystem = fs

  return new Promise((resolve, reject) => {
    compiler.run((err, stats) => {
      if (err) reject(err)
      if (stats.hasErrors()) reject(new Error(stats.toJson().errors))

      resolve(stats)
    })
  })
}
