import compiler from './compiler.js'

test('Inserts name and outputs JavaScript', async () => {
  const stats = await compiler('article/index.md')
  const output = stats.toJson().modules[0].source

  console.log(output)

  expect(output).toContain('<p>')
})
