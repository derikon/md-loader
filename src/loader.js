// import { getOptions } from 'loader-utils'
import report from 'vfile-reporter'
import unified from 'unified'
import markdown from 'remark-parse'
import remark2rehype from 'remark-rehype'
import images from './remark-images'
import html from 'rehype-stringify'
import FrontMatter from 'front-matter'
import prismjs from './remark-prism'

const stringify = (src) =>
  JSON.stringify(src)
    .replace(/\u2028/g, '\\u2028')
    .replace(/\u2029/g, '\\u2029')

export default function loader(source) {
  const callback = this.async()

  new Promise((resolve, reject) => {
    const parsed = FrontMatter(source)

    unified()
      .use(markdown)
      .use(images)
      .use(prismjs)
      .use(remark2rehype, null, { allowDangerousHTML: true })
      .use(html, { allowDangerousHTML: true })
      .process(parsed.body, function (err, file) {
        if (err) {
          reject(report(err || file))
        } else {
          const objString = `{content: ${stringify(
            String(file)
          )}, attributes: ${stringify(parsed.attributes)}}`
          resolve(objString)
        }
      })
  })
    .then((resolved) => callback(null, `export default ${resolved}`))
    .catch(callback)
}
