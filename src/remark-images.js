import visit from 'unist-util-visit'

export default function attacher({ include, exclude, prefix } = {}) {
  return (ast) => {
    visit(ast, 'image', (node, index, parent) => {
      node.type = 'html'
      node.value = `
       <img
          src="${node.url}"
          alt="${node.alt}"
          title="${node.title}"
          loading="lazy"
        />`
    })
  }
}
