const prismjs = require('./prismjs/index')

export default function attacher({ include, exclude, prefix } = {}) {
  return (ast) => prismjs(ast)
}
