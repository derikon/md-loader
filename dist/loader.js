"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = loader;

var _vfileReporter = _interopRequireDefault(require("vfile-reporter"));

var _unified = _interopRequireDefault(require("unified"));

var _remarkParse = _interopRequireDefault(require("remark-parse"));

var _remarkRehype = _interopRequireDefault(require("remark-rehype"));

var _remarkImages = _interopRequireDefault(require("./remark-images"));

var _rehypeStringify = _interopRequireDefault(require("rehype-stringify"));

var _frontMatter = _interopRequireDefault(require("front-matter"));

var _remarkPrism = _interopRequireDefault(require("./remark-prism"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import { getOptions } from 'loader-utils'
const stringify = src => JSON.stringify(src).replace(/\u2028/g, '\\u2028').replace(/\u2029/g, '\\u2029');

function loader(source) {
  const callback = this.async();
  new Promise((resolve, reject) => {
    const parsed = (0, _frontMatter.default)(source);
    (0, _unified.default)().use(_remarkParse.default).use(_remarkImages.default).use(_remarkPrism.default).use(_remarkRehype.default, null, {
      allowDangerousHTML: true
    }).use(_rehypeStringify.default, {
      allowDangerousHTML: true
    }).process(parsed.body, function (err, file) {
      if (err) {
        reject((0, _vfileReporter.default)(err || file));
      } else {
        const objString = `{content: ${stringify(String(file))}, attributes: ${stringify(parsed.attributes)}}`;
        const json = JSON.stringify({
          content: String(file),
          attributes: parsed.attributes
        }).replace(/\u2028/g, '\\u2028').replace(/\u2029/g, '\\u2029');
        resolve(objString);
      }
    });
  }).then(resolved => callback(null, `export default ${resolved}`)).catch(callback);
}