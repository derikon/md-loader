"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = attacher;

var _unistUtilVisit = _interopRequireDefault(require("unist-util-visit"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function attacher({
  include,
  exclude,
  prefix
} = {}) {
  return ast => {
    (0, _unistUtilVisit.default)(ast, 'image', (node, index, parent) => {
      node.type = 'html';
      node.value = `
       <img
          src="${node.url}"
          alt="${node.alt}"
          title="${node.title}"
          loading="lazy"
        />`;
    });
  };
}