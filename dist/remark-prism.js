"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = attacher;

const prismjs = require('./prismjs/index');

function attacher({
  include,
  exclude,
  prefix
} = {}) {
  return ast => prismjs(ast);
}